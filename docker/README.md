## Prerequisites

- [Docker](https://docs.docker.com/engine/install/)
- [pgAdmin](https://www.pgadmin.org/download/) / [DBeaver](https://dbeaver.io/download/) (DBeaver is recommended)
- [Robo3T](https://robomongo.org/download)
- Kitematic (optional)

## Setup

- Use the `.env-example` file to create a `.env` file in the same directory. 
- Update `/etc/hosts` and add the following mappings:

```
127.0.0.1       mongo1
127.0.0.1       mongo2
```

- Run `docker-compose up` from the `docker` folder.
- If Kitematic is installed, the images can be easily managed from there. If not then use the commands mentioned below to make any changes.

## Test 

- Connect with Mongo using Robo3T with SRV - `mongodb://mongo1:27017/test?replicaSet=rs0`
- Connect with PostgreSQL using DBeaver or pgAdmin with the details in the `.env` file.

## References

* [About Docker Compose](https://gabrieltanner.org/blog/docker-compose)
* [Docker Cheatsheet](https://devhints.io/docker-compose)

## Important Docker Commands

**Start docker container**

```
docker-compose up
```

**See all running docker containers**

```
docker ps
```

**Remove all docker containers**

```
docker-compose down
```

**Remove specific docker container**

```
docker kill container_id
```

**Clean images, builds, etc.**

```
docker system prune
```

**Terminal into a docker container**

```
docker exec -it container_id bash
```


